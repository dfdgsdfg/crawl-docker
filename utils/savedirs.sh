#!/bin/bash
cd /crawl-master
while [ $1 ]; do
  if [ $1 == "git" ]; then
		shift
		continue
	fi
	if [ ! -d "./crawl-$1" ]; then
		cp -r crawl-git crawl-$1
		chown -R "crawl:crawl" crawl-$1
		rm crawl-$1/saves/*.cs crawl-$1/saves/*.prf crawl-$1/saves/*/*.cs crawl-$1/saves/*/*.prf
		find crawl-$1/saves -type f -exec sh -c '>{}' \;
	else
		echo "crawl-$1 exists, doing nothing."
	fi
	shift
done
