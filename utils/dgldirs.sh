#!/bin/bash
while [ $1 ]; do
	FULL=$1
	CUT=$(echo "$1" | sed 's/0\.//')
	FOLDERS="/dgldir/inprogress/crawl-$CUT-sprint /dgldir/inprogress/crawl-$CUT-tut /dgldir/inprogress/crawl-$CUT-zotdef /dgldir/inprogress/crawl-$CUT/ /dgldir/rcfiles/crawl-$FULL /dgldir/data/crawl-$FULL-settings"
	mkdir -p $FOLDERS
	chown -R "crawl:crawl" $FOLDERS
	shift
done
