FROM buildpack-deps:wheezy
MAINTAINER TZer0

ENV LANG C.UTF-8

COPY ./docker/sources.list /etc/apt/sources.list

RUN apt-get update --fix-missing && \
    apt-get -y upgrade && \
    apt-get -y --force-yes install bison \
                                   bzip2 \
                                   flex \
                                   libbot-basicbot-perl \
                                   libfreetype6-dev \
                                   liblua5.1-0-dev \
                                   libncurses5-dev \
                                   libncursesw5-dev \
                                   libpcre3-dev \
                                   libpng-dev \
                                   libsqlite3-dev \
                                   libz-dev \
                                   locales \
                                   locales-all \
                                   lsof \
                                   ncurses-term \
                                   openssh-server \
                                   pkg-config \
                                   procps \
                                   python-minimal \
                                   python-pip \
                                   sudo \
                                   sqlite3 \
                                   vim && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

#RUN update-locale LANG=en_US.UTF-8
RUN pip install tornado==3.2.2

RUN useradd -m crawl && \
    useradd -m crawl-dev && \
    useradd -m terminal

###############
USER crawl-dev

# Configure here
ENV SERVERALIAS c---
ENV SERVERURLS crawl.----.com
ENV CRAWLVERSIONS git 0.15 0.14 0.13 0.12 0.11 0.10
ENV BOTNAME Doctell
ENV BOTGLYPH _
ENV TILES_PORT 8080
ENV LOCATION Dockerland
ENV MAINTAINER Docker McDock

# Don't touch these
ENV FOLDERS /crawl-master/webserver \
            /crawl-master/webserver/run \
            /crawl-master/webserver/sockets \
            /crawl-master/webserver/templates \
            /dgldir/data \
            /dgldir/dumps \
            /dgldir/morgue \
            /dgldir/rcfiles \
            /dgldir/ttyrec \
            /dgldir/data/menus \
            /dgldir/inprogress
ENV IP 0.0.0.0

RUN mkdir /home/crawl-dev/logs \
          /home/crawl-dev/run

WORKDIR /home/crawl-dev
RUN git clone -b szorg git://github.com/neilmoore/dgamelaunch.git && \
    git clone -b szorg git://github.com/neilmoore/dgamelaunch-config.git && \
    git clone git://github.com/neilmoore/sizzell.git

# Temporary - won't have to wait for everything to get pulled while testing.
WORKDIR /home/crawl-dev/dgamelaunch-config/crawl-build
RUN git clone git://gitorious.org/crawl/crawl.git crawl-git-repository

WORKDIR /home/crawl-dev/dgamelaunch-config/crawl-build/crawl-git-repository
RUN git submodule init && \
    git submodule update

###############
USER root

RUN chmod 666 /dev/ptmx
RUN echo "crawl-dev ALL=(root) NOPASSWD: /home/crawl-dev/dgamelaunch-config/bin/dgl, /sbin/install-trunk.sh, /sbin/install-stable.sh, /etc/init.d/webtiles, /sbin/remove-trunks.sh" >> /etc/sudoers

###############
USER crawl-dev

WORKDIR /home/crawl-dev/dgamelaunch/
RUN git checkout szorg && \
    ./autogen.sh --enable-debugfile --enable-sqlite --enable-shmem && \
    make VIRUS=1

###############
USER root

WORKDIR /home/crawl-dev/dgamelaunch/
RUN make install && \
    cp ee virus /bin

###############
USER crawl-dev

WORKDIR /home/crawl-dev/
RUN find . -type f -print0 | xargs -0 sed -i -e "s|cszo|$SERVERLIAS|gI" \
                                             -e "s|crawl.s-z.org|$SERVERURLS|gI" \
                                             -e "s|North America|$LOCATION|" \
                                             -e "s/Neil Moore (|amethyst)/$MAINTAINER/"

WORKDIR /home/crawl-dev/dgamelaunch-config
RUN sed -i -e "s|^export DGL_CHROOT=\/home\/crawl\/DGL|export DGL_CHROOT=\/|" dgl-manage.conf
RUN sed -i -e "s|^server_id.*$|server_id = \"$SERVERALIAS - Dungeon Crawl Stone Soup - $SERVERURLS\"|" \
           -e "s|^shed_uid.*$|shed_uid = $(id -u crawl)|" \
           -e "s|^shed_gid.*$|shed_gid = $(id -g crawl)|" dgamelaunch.conf
RUN sed -i -e "s|^bind_port = .*$|bind_port = $TILES_PORT|" \
           -e "s|^bind_address = \".*\"|bind_address = '$IP'|" \
           -e "s|(\".*\..*\..*\..*\", 80)|(bind_address, bind_port)|" \
           -e "s|uid =.*|uid = $(id -u crawl)|" \
           -e "s|gid =.*|gid = $(id -g crawl)|" \
           -e "/ssl_options = {/ {N;N;N;N;d;}" \
           -e "s|#ssl_options|ssl_options|" config.py
RUN echo config.py

WORKDIR /home/crawl-dev/sizzell
RUN sed -i -e "s|\/home\/crawl\/DGL\/|\/|" \
           -e "s|Sizzell|$BOTNAME|" \
           -e "s|my \$PREFIX = '%';|my \$PREFIX = '$BOTGLYPH';|" sizzell.pl

###############
USER root

COPY ./utils /utils
RUN cp /home/crawl-dev/dgamelaunch/dgamelaunch /bin/
RUN chown root:root /bin/dgamelaunch
RUN chmod 755 /bin/dgamelaunch
RUN echo "/bin/dgamelaunch" >> /etc/shells
RUN chsh -s /bin/dgamelaunch terminal
RUN usermod --password `openssl passwd terminal` terminal
RUN cat /utils/sshconf.txt >> /etc/ssh/sshd_config
#RUN /utils/postbuild.sh
#CMD /utils/launch.sh

VOLUME ["/usr/games/", "/dgldir", "/crawl-master"]
EXPOSE 8080 22
